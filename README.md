# PhaserGulpBoilerplate

Small Asteroid game using Phaser engine and TypeScript.

(Migrated from https://github.com/rbrochot/PhaserGulpBoilerplate as I found the perfect build :) )

Thanks and credits:
 - http://phaser.io/
 - https://github.com/rroylance/phaser-npm-webpack-typescript-starter-project
 - Space Shooter graphics by Kenney Vleugels (www.kenney.nl)

## TODO

 - dependency injection for "services": keyboard, factories, shared emitters (and maybe not shared, via not singleton dependency injection), etc.
    => no DI, just shared objects (DI would be such a mess with states management)
    => or maybe access them from phaser.game?
 - Simple menu (start, game over)
 - Use Phaser.Weapon?
 - Sounds
 - Fix particle reactor effects
    => use third party particle manager? or a custom one with a simple phaser group pool (which seems to be the base brick of every manager...)?
 - Pass collision manager into p2 mode (rotations and shapes)
 - Fix collision boxes
 - Fix weird canvas format in portrait mode (why does it have a scrollbar?)
