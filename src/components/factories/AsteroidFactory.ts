import Asteroid from '../sprites/Asteroid';
import * as Assets from '../../assets';

let RNG = new Phaser.RandomDataGenerator();

// Should only be a factory, and have kill method (and static emitter) in asteroid class,
//  but it seems overkill in this case...
export default class AsteroidFactory {
  private game: Phaser.Game;
  private asteroids: Phaser.Group;
  private rockEmitter: Phaser.Particles.Arcade.Emitter;

  constructor(game: Phaser.Game) {
    this.game = game;

    this.asteroids = this.game.add.group();
    this.asteroids.enableBody = true;
    this.asteroids.physicsBodyType = Phaser.Physics.ARCADE;

    this.rockEmitter = this.game.add.emitter(0, 0, 50);
    this.rockEmitter.makeParticles(Assets.Images.ImagesAsteroidsMeteorSmall.getName());
    this.rockEmitter.minParticleScale = 0.05;
    this.rockEmitter.maxParticleScale = 0.3;
    this.rockEmitter.setAlpha(1, 0, 500);
    this.rockEmitter.autoAlpha = true;
  }

  public getAsteroids() {
    return this.asteroids;
  }

  public killAsteroid(asteroid: Asteroid) {
    this.rockEmitter.x = asteroid.centerX;
    this.rockEmitter.y = asteroid.centerY;
    this.rockEmitter.explode(500, 5 * asteroid.size);

    // Spawn lesser asteroids if asteroid size > 1
    if (asteroid.size > 1) {
      for (let i = 0; i < 3; i++) {
        let newAsteroid = this.createAsteroid(asteroid.size - 1, new Phaser.Point(asteroid.centerX, asteroid.centerY));
      }
    }
    asteroid.kill();
  }

  public createAsteroid(size: number = 3, position: Phaser.Point = null) {
    if (position === null) {
      position = this.findViableAsteroidPosition();
    }

    let asteroid = this.asteroids.getFirstExists(false, true, position.x, position.y,  Assets.Images.ImagesAsteroidsMeteorBig.getName());
    asteroid.body.velocity.set(RNG.realInRange(-25, 25), RNG.realInRange(-25, 25));
    asteroid.body.angularVelocity = RNG.realInRange(0, 10);
    asteroid.size = size;
    asteroid.angle = RNG.angle();
    asteroid.anchor.set(0.5);
    asteroid.scale.set(size / 3);
    return asteroid;
  }

  public findViableAsteroidPosition() {
    let center = new Phaser.Point(400, 300);
    let position = new Phaser.Point();
    do {
      position.x = RNG.between(0, this.game.width);
      position.y = RNG.between(0, this.game.height);
    }
    while (Phaser.Point.distance(center, position) < 200);
    return position;
  }
}
