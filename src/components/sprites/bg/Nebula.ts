import * as Assets from '../../../assets';

let RNG = new Phaser.RandomDataGenerator();

export default class Nebula extends Phaser.Sprite {
  private velocity: Phaser.Point;
  private angularVelocity: number;

  constructor(game: Phaser.Game) {
    super(game, RNG.between(0, game.width), RNG.between(0, game.height), Assets.Images.ImagesAsteroidsBackgroundNebula.getName());
    this.scale.set(RNG.realInRange(0.5, 1));
    this.anchor.set(0.5);
    this.alpha = RNG.realInRange(0.4, 0.8);
    this.angle = RNG.angle();
    this.velocity = new Phaser.Point(RNG.realInRange(-0.2, 0.2), RNG.realInRange(-0.2, 0.2));
    this.angularVelocity = RNG.realInRange(-0.5, 0.5);

    game.stage.addChild(this);
  }

  public update(): Phaser.Sprite {
    this.x += this.velocity.x;
    this.y += this.velocity.y;
    this.angle += this.angularVelocity;
    return this;
  }

  public reset(): Phaser.Sprite {
    return this;
  }
}
