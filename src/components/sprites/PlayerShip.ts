import AsteroidGameControls from '../system/AsteroidGameControls';
import * as Assets from '../../assets';

// TODO Use DI for game, keyboard, and groups
export default class PlayerShip extends Phaser.Sprite {
  public game: Phaser.Game;
  private keyboard: AsteroidGameControls;
  private bullets: Phaser.Group;
  private bulletTime: number;
  private starEmitter: Phaser.Particles.Arcade.Emitter;
  private reactorEmitter: Phaser.Particles.Arcade.Emitter;
  private laserAudioSprite: Phaser.AudioSprite;

  constructor(game: Phaser.Game, keyboard: AsteroidGameControls, bullets: Phaser.Group) {
    super(game, 0, 0,  Assets.Images.ImagesAsteroidsPlayer.getName());
    this.scale.set(0.5);
    this.anchor.set(0.5);
    game.physics.arcade.enable(this);
    this.body.maxVelocity.set(250);

    this.keyboard = keyboard;
    this.bullets = bullets;
    this.bulletTime = 0;

    // TODO Use DI singleton?
    this.starEmitter = this.game.add.emitter(0, 0, 20);
    this.starEmitter.makeParticles(Assets.Images.ImagesAsteroidsStar.getName());
    this.starEmitter.minParticleScale = 0.1;
    this.starEmitter.maxParticleScale = 0.2;
    this.starEmitter.setAlpha(1, 0, 500);
    this.starEmitter.autoAlpha = true;
    this.addChild(this.starEmitter);

    this.reactorEmitter = this.game.add.emitter(0, 0, 35);
    this.reactorEmitter.makeParticles(Assets.Images.ImagesAsteroidsBackgroundSpeedLine.getName());
    this.reactorEmitter.minParticleScale = 0.1;
    this.reactorEmitter.maxParticleScale = 0.2;
    this.reactorEmitter.setAlpha(1, 0, 1000);
    this.reactorEmitter.autoAlpha = true;
    this.addChild(this.reactorEmitter);

    this.laserAudioSprite = this.game.add.audioSprite(Assets.Audiosprites.AudiospritesSfx.getName());

    game.stage.addChild(this);
  }

  public update(): Phaser.Sprite {
    // Rotation
    if (this.keyboard.left.isDown) {
      this.angle -= 3;
    }
    if (this.keyboard.right.isDown) {
      this.angle += 3;
    }

    // if (this.keyboard.left.isDown || this.keyboard.right.isDown) {
    //   let velocityDelta: Phaser.Point = this.game.physics.arcade.velocityFromAngle(this.angle - 90, 5);
    //   this.reactorEmitter.minParticleSpeed.set(velocityDelta.x, velocityDelta.y);
    //   this.reactorEmitter.maxParticleSpeed.set(velocityDelta.x, velocityDelta.y);
    //   this.reactorEmitter.setRotation(this.angle - 15, this.angle + 15);
    // }

    // Velocity
    if (this.keyboard.up.isDown) {
      let velocityDelta: Phaser.Point;
      velocityDelta = this.game.physics.arcade.velocityFromAngle(this.angle - 90, 5);
      this.body.velocity.add(velocityDelta.x, velocityDelta.y);
    }
    else if (this.keyboard.down.isDown) {
      let velocityDelta: Phaser.Point = this.game.physics.arcade.velocityFromAngle(this.angle - 90, -5);
      this.body.velocity.add(velocityDelta.x, velocityDelta.y);
    }

    if (this.keyboard.up.isDown && this.reactorEmitter.on === false) {
      this.reactorEmitter.start(false, 1000, 25, null);
    }
    else if (this.keyboard.up.isUp && this.reactorEmitter.on === true) {
      this.reactorEmitter.on = false;
    }

    // Shoot
    if (this.keyboard.space.isDown && this.game.time.now > this.bulletTime) {
      this.bulletTime = this.game.time.now + 500;
      let bullet: Phaser.Sprite = this.bullets.getFirstExists(false, true, this.x, this.y, Assets.Images.ImagesAsteroidsLaserGreen.getName());
      bullet.angle = this.angle;
      bullet.anchor.set(0.5);
      bullet.lifespan = 3000;
      this.game.physics.arcade.velocityFromAngle(this.angle - 90, 400, bullet.body.velocity);
      bullet.body.velocity.add(this.body.velocity.x, this.body.velocity.y);

      this.laserAudioSprite.play(Assets.Audiosprites.AudiospritesSfx.Sprites.Laser9.toString());
    }
    return this;
  }

  public reset(): Phaser.Sprite {
    super.reset(400 - this.width, 300 - this.height);
    this.body.velocity.set(0);
    return this;
  }

  public kill(): Phaser.Sprite {
    this.starEmitter.explode(500, 10);

    return super.kill();
  }
}
