import * as Assets from '../assets';
import PlayerShip from '../components/sprites/PlayerShip';
import Nebula from '../components/sprites/bg/Nebula';
import AsteroidFactory from '../components/factories/AsteroidFactory';
import Asteroid from '../components/sprites/Asteroid';
import AsteroidGameControls from '../components/system/AsteroidGameControls';

export default class Game extends Phaser.State {

  private score: number = 0;
  private level: number = 0;
  private isGameOver: boolean = false;

  private controls: AsteroidGameControls;
  private ship: PlayerShip;
  private asteroidFactory: AsteroidFactory;
  private asteroids: Phaser.Group;

  private backgroundSprite: Phaser.TileSprite;
  private nebulas: Phaser.Group;
  private bullets: Phaser.Group;

  constructor() {
    super();
  }

  public create() {
    this.backgroundSprite = this.game.add.tileSprite(0, 0, 800, 600, Assets.Images.ImagesAsteroidsBackgroundStarBackground.getName());

    this.controls = this.game.input.keyboard.createCursorKeys();
    this.controls.space = this.game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);

    this.nebulas = this.game.add.group();

    this.bullets = this.game.add.group();
    this.bullets.enableBody = true;
    this.bullets.physicsBodyType = Phaser.Physics.ARCADE;
    this.ship = new PlayerShip(this.game, this.controls, this.bullets);

    this.asteroidFactory = new AsteroidFactory(this.game);
    this.asteroids = this.asteroidFactory.getAsteroids();

    this.startNextLevel();
  }

  public update() {
    if (this.isGameOver && this.controls.space.isDown) {
      this.level = 0;
      this.score = 0;
      this.startNextLevel();
    }

    this.ship.update();
    this.backgroundSprite.tilePosition.x += 0.2;
    this.backgroundSprite.tilePosition.y += 0.2;

    // Collision detection
    this.game.physics.arcade.overlap(this.ship, this.asteroids, (ship, asteroid) => {
      this.ship.kill();
      this.isGameOver = true;
    }, null, this);
    this.game.physics.arcade.overlap(this.bullets, this.asteroids, (bullet, asteroid) => {
      if (!bullet.alive || !asteroid.alive) { return; }
      this.score += 100;

      this.asteroidFactory.killAsteroid(asteroid);

      // TODO find better laser impact
      bullet.kill();

      // Check alive asteroids, and pass to next level
      if (this.asteroids.getFirstAlive(false) === null) {
        this.startNextLevel();
      }
    }, null, this);

    // World-wrap
    this.game.world.wrap(this.ship, 0, true);
    this.asteroids.forEach((asteroid: Asteroid) => {
      this.game.world.wrap(asteroid, 0, true);
    }, this);
    this.bullets.forEach((bullet: Phaser.Sprite) => {
      this.game.world.wrap(bullet, 0, true);
    }, this);
  }

  public render() {
    // TODO find better score display
    this.game.debug.text('Score: ' + this.score, 620, 18, 'rgb(0,255,0)');

    if (DEBUG) {
      this.game.debug.bodyInfo(this.ship, 32, 32);

      this.game.debug.body(this.ship, 'rgba(0,255,0, 0.4)');
      this.asteroids.forEachAlive((asteroid: Phaser.Sprite) => {
        this.game.debug.body(asteroid, 'rgba(255,0,0, 0.4)');
      }, this);
    }
  }

  private startNextLevel() {
    this.clean();
    this.level++;
    for (let i = 0; i < this.level; i++) {
      this.asteroidFactory.createAsteroid();
    }
    let nebulaCount: number = new Phaser.RandomDataGenerator().between(0, 20);
    for (let i = 0; i < nebulaCount; i++) {
      this.nebulas.add(new Nebula(this.game));
    }
  }

  private clean() {
    this.isGameOver = false;
    this.asteroids.forEach((asteroid: Asteroid) => {
      asteroid.kill();
    }, this);
    this.bullets.forEach((bullet: Phaser.Sprite) => {
      bullet.kill();
    }, this);
    this.nebulas.removeAll(true);

    // Revive, center ship & set velocity to 0
    this.ship.reset();
  }
}
