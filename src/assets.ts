/* AUTO GENERATED FILE. DO NOT MODIFY. YOU WILL LOSE YOUR CHANGES ON BUILD. */

export namespace Images {
    export class ImagesAsteroidsBackgroundBackgroundColor {
        static getName(): string { return 'backgroundColor'; }

        static getPNG(): string { return require('assets/images/asteroids/Background/backgroundColor.png'); }
    }
    export class ImagesAsteroidsBackgroundNebula {
        static getName(): string { return 'nebula'; }

        static getPNG(): string { return require('assets/images/asteroids/Background/nebula.png'); }
    }
    export class ImagesAsteroidsBackgroundSpeedLine {
        static getName(): string { return 'speedLine'; }

        static getPNG(): string { return require('assets/images/asteroids/Background/speedLine.png'); }
    }
    export class ImagesAsteroidsBackgroundStarBackground {
        static getName(): string { return 'starBackground'; }

        static getPNG(): string { return require('assets/images/asteroids/Background/starBackground.png'); }
    }
    export class ImagesAsteroidsBackgroundStarBig {
        static getName(): string { return 'starBig'; }

        static getPNG(): string { return require('assets/images/asteroids/Background/starBig.png'); }
    }
    export class ImagesAsteroidsBackgroundStarSmall {
        static getName(): string { return 'starSmall'; }

        static getPNG(): string { return require('assets/images/asteroids/Background/starSmall.png'); }
    }
    export class ImagesAsteroidsEnemyShip {
        static getName(): string { return 'enemyShip'; }

        static getPNG(): string { return require('assets/images/asteroids/enemyShip.png'); }
    }
    export class ImagesAsteroidsEnemyUFO {
        static getName(): string { return 'enemyUFO'; }

        static getPNG(): string { return require('assets/images/asteroids/enemyUFO.png'); }
    }
    export class ImagesAsteroidsLaserGreen {
        static getName(): string { return 'laserGreen'; }

        static getPNG(): string { return require('assets/images/asteroids/laserGreen.png'); }
    }
    export class ImagesAsteroidsLaserGreenShot {
        static getName(): string { return 'laserGreenShot'; }

        static getPNG(): string { return require('assets/images/asteroids/laserGreenShot.png'); }
    }
    export class ImagesAsteroidsLaserRed {
        static getName(): string { return 'laserRed'; }

        static getPNG(): string { return require('assets/images/asteroids/laserRed.png'); }
    }
    export class ImagesAsteroidsLaserRedShot {
        static getName(): string { return 'laserRedShot'; }

        static getPNG(): string { return require('assets/images/asteroids/laserRedShot.png'); }
    }
    export class ImagesAsteroidsLife {
        static getName(): string { return 'life'; }

        static getPNG(): string { return require('assets/images/asteroids/life.png'); }
    }
    export class ImagesAsteroidsMeteorBig {
        static getName(): string { return 'meteorBig'; }

        static getPNG(): string { return require('assets/images/asteroids/meteorBig.png'); }
    }
    export class ImagesAsteroidsMeteorSmall {
        static getName(): string { return 'meteorSmall'; }

        static getPNG(): string { return require('assets/images/asteroids/meteorSmall.png'); }
    }
    export class ImagesAsteroidsPlayer {
        static getName(): string { return 'player'; }

        static getPNG(): string { return require('assets/images/asteroids/player.png'); }
    }
    export class ImagesAsteroidsPlayerDamaged {
        static getName(): string { return 'playerDamaged'; }

        static getPNG(): string { return require('assets/images/asteroids/playerDamaged.png'); }
    }
    export class ImagesAsteroidsPlayerLeft {
        static getName(): string { return 'playerLeft'; }

        static getPNG(): string { return require('assets/images/asteroids/playerLeft.png'); }
    }
    export class ImagesAsteroidsPlayerRight {
        static getName(): string { return 'playerRight'; }

        static getPNG(): string { return require('assets/images/asteroids/playerRight.png'); }
    }
    export class ImagesAsteroidsShield {
        static getName(): string { return 'shield'; }

        static getPNG(): string { return require('assets/images/asteroids/shield.png'); }
    }
    export class ImagesAsteroidsStar {
        static getName(): string { return 'star'; }

        static getPNG(): string { return require('assets/images/asteroids/star.png'); }
    }
    export class ImagesBackgroundTemplate {
        static getName(): string { return 'background_template'; }

        static getPNG(): string { return require('assets/images/background_template.png'); }
    }
}

export namespace Spritesheets {
    export class SpritesheetsMetalslugMummy374518 {
        static getName(): string { return 'metalslug_mummy.[37,45,18,0,0]'; }

        static getPNG(): string { return require('assets/spritesheets/metalslug_mummy.[37,45,18,0,0].png'); }
        static getFrameWidth(): number { return 37; }
        static getFrameHeight(): number { return 45; }
        static getFrameMax(): number { return 18; }
        static getMargin(): number { return 0; }
        static getSpacing(): number { return 0; }
    }
}

export namespace Atlases {
    enum AtlasesPreloadSpritesArrayFrames {
        PreloadBar = <any>'preload_bar.png',
        PreloadFrame = <any>'preload_frame.png',
    }
    export class AtlasesPreloadSpritesArray {
        static getName(): string { return 'preload_sprites_array'; }

        static getJSONArray(): string { return require('assets/atlases/preload_sprites_array.json'); }

        static getPNG(): string { return require('assets/atlases/preload_sprites_array.png'); }

        static Frames = AtlasesPreloadSpritesArrayFrames;
    }
    enum AtlasesPreloadSpritesHashFrames {
        PreloadBar = <any>'preload_bar.png',
        PreloadFrame = <any>'preload_frame.png',
    }
    export class AtlasesPreloadSpritesHash {
        static getName(): string { return 'preload_sprites_hash'; }

        static getJSONHash(): string { return require('assets/atlases/preload_sprites_hash.json'); }

        static getPNG(): string { return require('assets/atlases/preload_sprites_hash.png'); }

        static Frames = AtlasesPreloadSpritesHashFrames;
    }
    enum AtlasesPreloadSpritesXmlFrames {
        PreloadBar = <any>'preload_bar.png',
        PreloadFrame = <any>'preload_frame.png',
    }
    export class AtlasesPreloadSpritesXml {
        static getName(): string { return 'preload_sprites_xml'; }

        static getPNG(): string { return require('assets/atlases/preload_sprites_xml.png'); }

        static getXML(): string { return require('assets/atlases/preload_sprites_xml.xml'); }

        static Frames = AtlasesPreloadSpritesXmlFrames;
    }
}

export namespace Audio {
    export class AudioMusic {
        static getName(): string { return 'music'; }

        static getAC3(): string { return require('assets/audio/music.ac3'); }
        static getM4A(): string { return require('assets/audio/music.m4a'); }
        static getMP3(): string { return require('assets/audio/music.mp3'); }
        static getOGG(): string { return require('assets/audio/music.ogg'); }
    }
}

export namespace Audiosprites {
    enum AudiospritesSfxSprites {
        Laser1 = <any>'laser1',
        Laser2 = <any>'laser2',
        Laser3 = <any>'laser3',
        Laser4 = <any>'laser4',
        Laser5 = <any>'laser5',
        Laser6 = <any>'laser6',
        Laser7 = <any>'laser7',
        Laser8 = <any>'laser8',
        Laser9 = <any>'laser9',
    }
    export class AudiospritesSfx {
        static getName(): string { return 'sfx'; }

        static getAC3(): string { return require('assets/audiosprites/sfx.ac3'); }
        static getJSON(): string { return require('assets/audiosprites/sfx.json'); }
        static getM4A(): string { return require('assets/audiosprites/sfx.m4a'); }
        static getMP3(): string { return require('assets/audiosprites/sfx.mp3'); }
        static getOGG(): string { return require('assets/audiosprites/sfx.ogg'); }

        static Sprites = AudiospritesSfxSprites;
    }
}

export namespace GoogleWebFonts {
    export const Barrio: string = 'Barrio';
}

export namespace CustomWebFonts {
    export class Fonts2DumbWebfont {
        static getName(): string { return '2Dumb-webfont'; }

        static getFamily(): string { return '2dumbregular'; }

        static getCSS(): string { return require('!file-loader?name=assets/fonts/[name].[ext]!assets/fonts/2Dumb-webfont.css'); }
        static getEOT(): string { return require('!file-loader?name=assets/fonts/[name].[ext]!assets/fonts/2Dumb-webfont.eot'); }
        static getSVG(): string { return require('!file-loader?name=assets/fonts/[name].[ext]!assets/fonts/2Dumb-webfont.svg'); }
        static getTTF(): string { return require('!file-loader?name=assets/fonts/[name].[ext]!assets/fonts/2Dumb-webfont.ttf'); }
        static getWOFF(): string { return require('!file-loader?name=assets/fonts/[name].[ext]!assets/fonts/2Dumb-webfont.woff'); }
    }
}

export namespace BitmapFonts {
    export class FontsFontFnt {
        static getName(): string { return 'font_fnt'; }

        static getFNT(): string { return require('assets/fonts/font_fnt.fnt'); }
        static getPNG(): string { return require('assets/fonts/font_fnt.png'); }
    }
    export class FontsFontXml {
        static getName(): string { return 'font_xml'; }

        static getPNG(): string { return require('assets/fonts/font_xml.png'); }
        static getXML(): string { return require('assets/fonts/font_xml.xml'); }
    }
}

export namespace JSON {
    class IExistSoTypeScriptWillNotComplainAboutAnEmptyNamespace {}
}

export namespace XML {
    class IExistSoTypeScriptWillNotComplainAboutAnEmptyNamespace {}
}

export namespace Text {
    class IExistSoTypeScriptWillNotComplainAboutAnEmptyNamespace {}
}

export namespace Scripts {
    export class ScriptsBlurX {
        static getName(): string { return 'BlurX'; }

        static getJS(): string { return require('assets/scripts/BlurX.js'); }
    }
    export class ScriptsBlurY {
        static getName(): string { return 'BlurY'; }

        static getJS(): string { return require('assets/scripts/BlurY.js'); }
    }
}
export namespace Shaders {
    export class ShadersPixelate {
        static getName(): string { return 'pixelate'; }

        static getFRAG(): string { return require('assets/shaders/pixelate.frag'); }
    }
}
export namespace Misc {
    class IExistSoTypeScriptWillNotComplainAboutAnEmptyNamespace {}
}
